## Attendees

* Andrew Reynolds
* Nick Haggin
* Jon Gorman
* Bryan Jonker
* Rebecca Byrd
* Michael McKelvey
* Jeff Dimpsey
* Dave Riddle
* Jon Roma

## Agenda

* Introductions
* Status update
* Identify service components
* Advance trial discussion

## Status

**Project Sponsorship:** Chuck is leaving the U of I. He and Dave M are working on smooth sponsor handoff and various purchasing options. Project is still on.

**Licensing:** GitHub Enterprise trial license is ready for use. GitLab Enterprise license still in negotiations. Trial licenses are valid for 45 days.

**General Rollout Strategy:** Admin team would like to evaluate hosting and integration issues over winter break. Advance team should review accessibility and known use cases to make sure our candidates meet minimum viable product requirements.

After infrastructure and advance trials, the 45-day trial window will be too short for fair evaluation by campus users, especially if trials begin over break. Since both companies' license terms are flexible, the most likely scenario is that we run a small-scale beta service to allow campus input before final purchasing decisions are made.

## Core/Reviewable Components

Before beginning advance trials, we should identify the basic service components. Focus on the web interface, since that will be the initial interaction for most users.

(Proposed) The web interface must support:

* Git
    * Basic functions (init, branch, commit, log, merge, fork)
* Account Management
    * AD integration is assumed
    * Personal accounts
    * Unofficial organizations
    * Campus units
* Team Management
    * Project permissions by role and/or account
* Issue Tracker
    * Includes comment/collaboration tools, such as pull requests
* API
    * Not strictly part of the web interface, but is a known requirement
* Documentation
    * Tutorials
    * Service documentation
    * API documentation

Additional known scenarios:

* Coursework hand-in (CS, Engineering)
* Large file support (Research groups)

## Advance Trials

### Division of Labor

Propose dividing into three affinity groups:

* Admin/Infrastructure: focus on architecture and service management
* Functional: Evaluate products against use cases and service requirements to avoid obvious failures
* Accessibility: Evaluate service requirements against accessible purchasing guide. Identify critical problems and propose remediation.

Affinity groups may be fluid. The following expressed strong interest in a particular area.

**Infrastructure:** Nick Haggin

**Functional:** Dave Riddle, Bryan Jonker (Documentation)

**Accessibility:** Rebecca Byrd, Michael McKelvey

### Process

In general, treat evaluation tasks as project issues.

Add evaluation documents in a new branch on the git-use-cases repo, and submit a merge request documenting the review process.

Add new issues as problems or further evaluations are identified.
