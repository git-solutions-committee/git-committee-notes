# Solutions Development Committee Meeting: 2015-10-29

## Agenda

1. Introductions
2. Background Info & Goals
3. Status Update
    * Outreach
    * Beta Service
4. Next Steps
    * Use Cases
    * Communications & Outreach
5. Meeting time and location

## Attendees
* Chris Maden (crism)
* Bryan Jonker (jonker)
* Dave Mussulman (mussulma)
* Jon Gorman (jtgorman)
* Andrew Reynolds (areynold)
* Michael McKelvey (mmckelve)
* Rebecca Byrd (rabyrd)

## Notes

### Product Evaluations
Engineering IT is planning to evaluate Github Enterprise and GitLab
    * *crism:* Any plan to evaluate Atlassian products?

Products under consideration have a 45-day eval period.
Need to coordinate outreach prep with service setup prep so both are ready to go. Don't want to waste our eval window.

All products have similar features and are able to handle standard use cases.
Need to find the edge cases, and volunteers who are interested in A-B testing.

Planning to roll out a campuswide instance.

Not clear yet what the licensing terms will look like. Publicly displayed license options fall short of campus requirements, but both services were designed to be scalable enterprise solutions. Will need to work with vendors to examine issues like number of servers allowed, how seats are counted (students/faculty vs. staff), etc.

### Outreach

*areynold, jonker:* Began a targeted, small-scale round of interviews to gauge interest. Generated a lot of requirements and further leads, even without touching large groups like CCSP.

Need better-defined message, goals, and points of contact before continuing. Risk of overpromising.

*crism:* Is this a required service? Need to address pressure (or perception of pressure) for units to convert to a service that may not fit their needs. Especially true for units that are already invested in alternate solutions (e.g., Help Desks that require ticket trackers, not just project issue queues).

Proposal: conduct evaluations and meetings as open source project. Track meeting notes and files in git. Handle action items and requests via project issue. Allows public input into process, and provides an active project to help evaluate service features.


## Action Items

1. Create Engr Beta Project for use cases and whatnot (areynold)
2. Set up project and group for committee (areynold)
3. Send service request for Gitlab Pages (areynold)
4. Schedule meeting for late-november (areynold)
