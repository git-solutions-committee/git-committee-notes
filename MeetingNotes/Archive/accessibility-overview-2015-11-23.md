# Accessibility Testing Overview

## Attendees

* Andrew Reynolds
* Keith Hays (Tech Services)

## Resources

* [Accessibility Requirements for Procured Software](../Resources/Accessibility Requirements for Procured Software v1.7.pdf)
* **Screenreaders:**
    * NVDA + Firefox
    * JAWS + Internet Explorer
    * Voiceover + Mac
* **Browser Tools:**
    * [AInspector Sidebar](https://ainspector.github.io/)
    * [Functional Accessibility Evaluator (FAE)](http://fae20.cita.illinois.edu/)

## Recommendations

### Priorities

* Can I perform all critical functions with the keyboard?
* Is the interface logical and understandable?
* Is the navigation scheme usable?
* Is the vendor responsive?
* Check keyboard navigation, then Mandatory Accessibility Requirements (Section 1.2 of procurement doc), then Minimum Functional Accessibility Checklist and Vendor Assessment Checklist (Appendix 3 of procurement doc)

### Meeting Notes

One interface is best. For our purposes, it would be best to focus on the web interface.

Identify feature priorities and weight them accordingly. Don't sacrifice features, but if features are not accessible, we will need to create an alternate access plan and review the results and plan periodically.

Assuming correct structure, most web interfaces are generally accessible. AJAX and drag-and-drop functions are common failure points.

Use browser tools to evaluate markup. It's possible, but not ideal, to work around some errors (e.g., skipping from h1 to h3 with no h2). A moment of confusion is better than a complete failure.

**Key Question:** Can I perform all critical functions with the keyboard?

Avoid keyboard shortcuts, as they can interfere with key bindings on accessibility tools. Standard tab, arrow, space/enter, escape movement is preferred.

Test keyboard navigation with a screenreader! The results may be dramatically different than on-screen display.

An application that fails keyboard navigation cannot be considered accessible.

**Key Question:** Is the interface logical and understandable?

* Does the UI use proper header structure? (check with AInspector Sidebar)
* Does the site overuse absolute positioning, making structure hard to follow? (Turn off stylesheets to check.)
* Are lists tagged as lists programmatically?
* Are navigation elements and page regions tagged appropriately?
* Is the site robust? Does it use deprecated or bleeding edge syntax?

**Key Question:** Is the navigation scheme usable?

* Do I know where I am at all times?
* Can I do what I came to do?

See [Accessibility Requirements, Appendix 2](../Resources/Accessibility Requirements for Procured Software v1.7.pdf)] for details.

Look hard at click & drag and graph functions--these are frequently inaccessible.

**Key Question:** How responsive is the vendor?

* Does the vendor have a dedicated accessibility contact?
* Has the vendor completed a [Voluntary Product Accessibility Template (VPAT)](http://www.state.gov/m/irm/impact/126343.htm)? Many have not, especially if they don't receive federal funding.
    * Keith is working on a [simplified VPAT](http://192.17.26.249/vpat/). Still alpha, and only reachable from campus.

### What if the products all fail?

Prioritize features before testing, and weight results accordingly. A failure in a trivial component (e.g., GitLab's logo hover animation) is not as severe as a failure in a required feature.

Show due diligence--document product searches, evaluations, and results.

Attempt vendor remediation:
* Ask vendor to correct flaws (don't forget to re-test)
* If we have the source code, apply a local fix
    * GitLab uses Bootstrap. Consider PayPal's Bootstrap accessibility shim

Put out RFP. If *no* products are accessible, requiring accessibility prior to purchase may constitute an undue burden exemption

Show your work! Documentation will be critical in showing due diligence.
