# Committee Status

*Originally reported by @mussulma*

In December 2015, committee sponsor Chuck Thompson attended an informational meeting for IT Power Plant leads,
where an OBFS representative presented common pitfalls that could endanger smooth implementation.

The git solutions committee was at risk on several issues, most notably the recurring cost RFP threshold and
the use of reserved words in the committee name.

**Recurring costs:** A campus purchase requires an RFP if the one-time cost exceeds a certain threshold, or if
the recurring costs over 5 years exceed a higher threshold. The proposed solutions were expected to exceed the
5-year threshold.

**Reserved words:** In campus service parlance, a "pilot" program is one that has been thoroughly vetted and is
nearly operational, not a malleable proof of concept as used by this committee.

Although no purchases had been made and no services run, the solutions committee chose to step back until it could
work within the correct service and purchasing frameworks.

The original committee notes are left to help future committees identify potential service questions.
