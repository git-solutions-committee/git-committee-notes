IDEs used: IntelliJS, Eclipse, Netbase, Visual Studio

Dave has been set up for licensing. 45 day trial for GitHub and GitLab. Likes to start with GitHub trial, especially with break coming up. This is the advance trial. 

What is the rollout strategy? If we go through the 45 day eval period, then we can start a beta-period and scale up/down licensing as needed. From there, we can determine real needs (training materials). 

Number of repositories: depends on licensing. Concerned that platinum license is not enough. 

Location: locally hosted (to avoid issues with FERPA and to make more attractive to professors). 

We've done nothing with the idea of local professors sharing with people outside of UIUC. Maybe something in the future, need to talk with Dave about this. 

What do we want to test? What are the "must-haves". For example, CI --> is this required? How about large file support, like file support or VM? Accessibility? Can we store security information in GitHub? How about FERPA and HIPAA information? 

Security is working on the policy on what we can store, but we can make recommendations? 

Core components for the advance group:
* Focus on Web Interface. 
* Collaboration tools (submit pull request, tie issues)
* Issue queue
* Account management is solid (for users, official nodes/groups and unofficial groups). Will this scale well? 
* Team management (what does permissions look like). This needs to be self-service. 
* Admin group and AD integration (precursor to account and team management)
* Populating class projects from a class roster, where people haven't logged in
* Documentation is solid and visible. 
 
API is not as important (this isn't testable, and will be used by a smaller group). 

Look at differences between GitHub and GitLab in terms of account management. 

So, write down what we plan to test, and determine what succeeds and fails. 
Tech Services may require needs that we don't, because they service hundreds and hundreds of users / services

Naming convention. Have some type of convention? You can have multiple repos for a single project, so if we have a license for a limited number of repos, then we probably are looking at the wrong license. We need to talk with Dave about this. 

Note -- with Git, you can delete data (unlike Subversion). 
It's very important that this is useful for non-tech users. Give them a list of things to avoid. 

CS Professors are on the list to start using this. 

Do we want to start with source control? Or document control? For the 45 day advance trial, we focus on source control. 


Tasks?
* ???: Administration 
* ???: Backend
* ???: Functional 
* Michael: Accessibility
* Bryan: Documentation and New User