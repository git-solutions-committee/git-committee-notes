# Git Committee Notes (Archival)

This project contains **ARCHIVAL** [meeting notes and administrative documents](https://gitlab-beta.engr.illinois.edu/git-solutions-committee/git-committee-notes/tree/master) for a Git Service investigative team.
Evaluations were halted before they began in December 2015 in light of campus purchase and service policy information provided by Chuck Thompson and @mussulma.
See the [Meeting Notes README](MeetingNotes/Archive/README.md) for more details.

Given the continued in a campus git service, these notes will be archived in case they can be of use to future committee members.

Use cases, user stories, and practical evaluations can be found in the [Git Use Cases](https://gitlab-beta.engr.illinois.edu/git-solutions-committee/git-use-cases) project.

For more information on using these projects, see the [Contribution Guide](CONTRIBUTING.md).

## Project Status

* Inaugural meetings complete
* Evaluation group decommissioned.

## Original Committee Members

### Project Sponsor

* Chuck Thompson (College of Engineering)

### Advance Planning Team

* Andrew Reynolds (Beckman Institute)(Team Lead)
* Rebecca Byrd (College of Education)
* Jeff Dimpsey (Technology Services)
* Jon Gorman (Library)
* Tom Habing (Library)
* Steven Holland (Office of the Vice Chancellor for Research)
* Bryan Jonker (College of Education)
* Chris Maden (Library & Information Science)
* Michael McKelvey (College of Education)
* David Riddle (Technology Services)
* Jon Roma (Technology Services)

### Infrastructure Team

* Dave Mussulman (College of Engineering)(Team Lead)
* Joel Franzen (College of Engineering)
* Nick Haggin (Technology Services)
