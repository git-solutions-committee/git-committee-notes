# Licensing and Intellectual Property Resources

## University

* [Copyright Education & Consultation Program](http://blogs.cites.illinois.edu/library-copyright/)
* [Office of Technology Management](http://otm.illinois.edu/)
* [Scholarly Commons Copyright Service](http://www.library.illinois.edu/sc/services/copyright/index.html)

## General

* [Choose an Open Source License](http://choosealicense.com)
