# Getting Started

We are treating all projects under the Git Solutions Committee umbrella as open source projects. All university faculty, staff, and students are welcome to participate in testing and may contribute to project repositories.

By using a live project for evaluation, we believe participants will have a better chance to evaluate common workflow features like merge requests and issue discussion.

## Contributing to the Project

All documents and files are contained in project git repositories, and are arranged by topic. Outstanding requests and open discussion topics are managed through the project issue queues.

Project issues are the simplest way to contribute to the initial evaluation. If an open issue affects you or your unit, add a comment explaining how. If you have a use case that hasn't been discussed, open a new issue and tell us what we've missed. If you find a fatal flaw in the service models, we want to hear that, too.

You may also want to help us evaluate draft documentation by reviewing and commenting on merge requests.

Finally, you may contribute directly by cloning the project git repository, writing or editing a document, and submitting a merge request through the project web interface.

**General Guidelines:**
* Always create a new git branch before editing project files.
* All documents should be written in Markdown so they can be read as both text files and web pages.
* Please use merge requests instead of editing documents through the web interface.
* Please do not approve your own merge request.
* Keep comment threads professional. The solutions committee reserves the right to edit, remove, or close comments that do not contribute to the discussion.

## Getting Help

Service guidelines and links to documentation can be found in the [Resources directory](https://gitlab-beta.engr.illinois.edu/git-solutions-committee/git-meeting-notes/tree/master/Resources) of the git-meeting-notes project.
